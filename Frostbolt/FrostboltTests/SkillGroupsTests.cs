﻿using Frostbolt.SkillGems;
using Frostbolt.State;
using Frostbolt.State.Components;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrostboltTests
{
    public class SkillGroupsTests
    {
        [Test]
        public void CanBuildSkillGroupFromUnlinkedFrostbolt()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();

            // Expected
            SkillGroups expected = new SkillGroups { new List<ISkillGem> { new FrostBolt() } };

            // When
            SkillGroups actual = SkillGroupsFactory.CreateSkillGroups(gameState);

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void CanBuildSkillGroupsFromUnlinkedFrostboltIceNova()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.GemSlots[2] = new IceNova();

            // Expected
            SkillGroups expected = new SkillGroups { 
                new List<ISkillGem> { new FrostBolt() },
                new List<ISkillGem> { new IceNova() }
            };

            // When
            SkillGroups actual = SkillGroupsFactory.CreateSkillGroups(gameState);

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void CanBuildSkillGroupsFromLinkedFrostbolt()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.GemSlots[4] = new GreaterMultipleProjectileSupport();

            // Expected
            SkillGroups expected = new SkillGroups {
                new List<ISkillGem> { new FrostBolt(), new GreaterMultipleProjectileSupport() }
            };

            // When
            SkillGroups actual = SkillGroupsFactory.CreateSkillGroups(gameState);

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void CanBuildSkillGroupsFromLinkedFrostboltIceNova()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.GemSlots[2] = new IceNova();
            gameState.GemSlots[4] = new GreaterMultipleProjectileSupport();
            gameState.GemSlots[5] = new ControlledDestructionSupport();

            // Expected
            SkillGroups expected = new SkillGroups {
                new List<ISkillGem> { new FrostBolt(), new GreaterMultipleProjectileSupport(), new ControlledDestructionSupport() },
                new List<ISkillGem> { new IceNova(), new ControlledDestructionSupport() }
            };

            // When
            SkillGroups actual = SkillGroupsFactory.CreateSkillGroups(gameState);

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

    }
}
