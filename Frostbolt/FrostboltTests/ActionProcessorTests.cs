﻿using Frostbolt.Engine;
using Frostbolt.SkillGems;
using Frostbolt.State;
using Frostbolt.State.Components;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrostboltTests
{
    public class ActionProcessorTests
    {
        [Test]
        public void CanCastFrostbolt()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.Keys["mouse right"] = "frostbolt";

            string[] instructions = new string[] { "press", "mouse", "right" };

            // Expected
            ActionResult expected = new ActionResult()
            {
                { "projectiles", "1" },
                { "damage", "2000" },
                { "cast speed", "0.75" }
            };


            // When
            new ActionProcessor().Process(instructions, gameState);

            // Then
            ActionResult actual = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void CanCastFrostbolt_GMP_CD()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.GemSlots[3] = new GreaterMultipleProjectileSupport();
            gameState.GemSlots[5] = new ControlledDestructionSupport();
            gameState.Keys["mouse right"] = "frostbolt";

            string[] instructions = new string[] { "press", "mouse", "right" };

            // Expected
            ActionResult expected = new ActionResult()
            {
                { "projectiles", "5" },
                { "angles", "-10, -5, 0, 5, 10" },
                { "damage", "2100" },
                { "base critical damage", "-100" },
                { "cast speed", "0.75" }
            };

            // When
            new ActionProcessor().Process(instructions, gameState);

            // Then
            ActionResult actual = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void CanCastIceNovaOrFrostbolt()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new IceNova();
            gameState.GemSlots[2] = new FrostBolt();
            gameState.Keys["key t"] = "icenova";
            gameState.Keys["key x"] = "frostbolt";

            string[] instructionsNova = new string[] { "press", "key", "t" };
            string[] instructionsBolt = new string[] { "press", "key", "x" };

            // Expected
            ActionResult expectedNova = new ActionResult()
            {
                { "casts", "1" },
                { "damage", "800" },
                { "cast speed", "0.7" }
            };

            ActionResult expectedBolt = new ActionResult()
            {
                { "projectiles", "1" },
                { "damage", "2000" },
                { "cast speed", "0.75" }
            };


            // When
            new ActionProcessor().Process(instructionsNova, gameState);

            // Then
            ActionResult actualNova = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expectedNova, actualNova);

            // When
            new ActionProcessor().Process(instructionsBolt, gameState);

            // Then
            ActionResult actualBolt = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expectedBolt, actualBolt);
        }

        [Test]
        public void CanCastIceNova_GmpEchoCd()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new ControlledDestructionSupport();
            gameState.GemSlots[2] = new GreaterMultipleProjectileSupport(); // this one has no influence - not a projectile
            gameState.GemSlots[4] = new IceNova();
            gameState.GemSlots[5] = new SpellEchoSupport();

            gameState.Keys["mouse middle"] = "icenova";

            string[] instructionsNova = new string[] { "press", "mouse", "middle" };

            // Expected
            ActionResult expected = new ActionResult()
            {
                { "casts", "2" },                       // Echo: *2
                { "damage", "1008" },                   // Damage: 800 (base) * 0.9 (echo) * 1.4 (cd) = 
                { "base critical damage", "-100" },     // CD
                { "cast speed", "0.41" }                // Echo /1.7
            };


            // When
            new ActionProcessor().Process(instructionsNova, gameState);

            // Then
            ActionResult actual = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expected, actual);

        }

        [Test]
        public void CanCastFrostbolt_Echo()
        {
            // Given
            GameState gameState = new GameState();
            gameState.GemSlots[1] = new FrostBolt();
            gameState.GemSlots[2] = new SpellEchoSupport();
            gameState.Keys["mouse right"] = "frostbolt";

            string[] instructions = new string[] { "press", "mouse", "right" };

            // Expected
            ActionResult expected = new ActionResult()
            {
                { "projectiles", "2" },
                { "damage", "1800" },
                { "cast speed", "0.44" }
            };


            // When
            new ActionProcessor().Process(instructions, gameState);

            // Then
            ActionResult actual = gameState.ActionResult;

            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}
