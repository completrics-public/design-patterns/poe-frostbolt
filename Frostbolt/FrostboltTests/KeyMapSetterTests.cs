﻿using Frostbolt.Engine;
using Frostbolt.State;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace FrostboltTests
{
    public class KeyMapSetterTests
    {
        [Test]
        public void SetRmbToFrostbolt()
        {
            // Given
            GameState gameState = new GameState();
            string[] instructions = new string[] { "setbutton", "mouse", "right", "frostbolt" };

            // When
            new KeyMapSetter().SetKey(instructions, gameState);

            // Then
            Assert.AreEqual(gameState.Keys["mouse right"], "frostbolt");
        }

        [Test]
        public void SetKeyTToIceNova()
        {
            // Given
            GameState gameState = new GameState();
            string[] instructions = new string[] { "setbutton", "key", "t", "icenova" };

            // When
            new KeyMapSetter().SetKey(instructions, gameState);

            // Then
            Assert.AreEqual(gameState.Keys["key t"], "icenova");
        }

        [Test]
        public void UnsetKeysHaveNothing()
        {
            // Given
            GameState gameState = new GameState();
            
            // Then
            Assert.AreEqual(gameState.Keys["random string"], string.Empty);
        }
    }
}
