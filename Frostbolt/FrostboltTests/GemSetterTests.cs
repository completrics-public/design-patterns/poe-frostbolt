using Frostbolt.Engine;
using Frostbolt.SkillGems;
using Frostbolt.State;
using NUnit.Framework;

namespace FrostboltTests
{
    public class GemSetterTests
    {

        [Test]
        public void SetFrostBoltAtIndex1()
        {
            // Given

            GameState gameState = new GameState();
            string[] instruction = new string[] { "setgem", "1", "frostbolt" };

            // When
            new GemSetter().SetGem(instruction, gameState);

            // Then
            Assert.IsTrue(gameState.GemSlots[1] is FrostBolt);
        }

        [Test]
        public void SetGmpAtIndex2()
        {
            // Given

            GameState gameState = new GameState();
            string[] instruction = new string[] { "setgem", "2", "gmp" };

            // When
            new GemSetter().SetGem(instruction, gameState);

            // Then
            Assert.IsTrue(gameState.GemSlots[2] is GreaterMultipleProjectileSupport);
        }

        [Test]
        public void SetIceNovaAtIndex3()
        {
            // Given

            GameState gameState = new GameState();
            string[] instruction = new string[] { "setgem", "3", "icenova" };

            // When
            new GemSetter().SetGem(instruction, gameState);

            // Then
            Assert.IsTrue(gameState.GemSlots[3] is IceNova);
        }

        [Test]
        public void SetSpellEchoAtIndex4()
        {
            // Given

            GameState gameState = new GameState();
            string[] instruction = new string[] { "setgem", "4", "spellecho" };

            // When
            new GemSetter().SetGem(instruction, gameState);

            // Then
            Assert.IsTrue(gameState.GemSlots[4] is SpellEchoSupport);
        }

        [Test]
        public void SetControlledDestructionAtIndex5()
        {
            // Given

            GameState gameState = new GameState();
            string[] instruction = new string[] { "setgem", "5", "cd" };

            // When
            new GemSetter().SetGem(instruction, gameState);

            // Then
            Assert.IsTrue(gameState.GemSlots[5] is ControlledDestructionSupport);
        }

        [Test]
        public void UnallocatedGemsAreUnallocated()
        {
            // Given
            // When
            GameState gameState = new GameState();

            // Then
            Assert.IsTrue(gameState.GemSlots[0] is UnallocatedSkillGem);
        }
    }
}