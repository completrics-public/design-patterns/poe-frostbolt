﻿using Frostbolt.Engine;
using Frostbolt.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.Ui
{
    public class UiMessageDisplayer
    {
        public string Display(GameState gameState)
        {
            if (string.IsNullOrEmpty(gameState.Message) == false)
            {
                string toDisplay = gameState.Message;
                gameState.Message = string.Empty;
                return toDisplay;
            }

            return "meow";
        }
    }
}
