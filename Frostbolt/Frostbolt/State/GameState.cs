﻿using Frostbolt.SkillGems;
using Frostbolt.State.Components;
using Frostbolt.State.Components.Equipment;
using Frostbolt.Ui;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.State
{
    public class GameState
    {
        public GameState()
        {
            Message = string.Empty;
            Finished = false;
            GemSlots = new BodyArmour();
            Keys = new KeyMap();
            ActiveSkillGroups = new SkillGroups();
            ActionResult = new ActionResult();
        }

        public bool Finished { get; set; }
        public string Message { get; set; }
        public BodyArmour GemSlots { get; }
        public KeyMap Keys { get; }
        public SkillGroups ActiveSkillGroups { get; set; }
        public ActionResult ActionResult { get; set; }

        public void BuildSkillGroupsFromActiveGems() { this.ActiveSkillGroups = SkillGroupsFactory.CreateSkillGroups(this); }

    }
}
