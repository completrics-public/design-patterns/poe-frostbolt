﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.State.Components
{
    public class KeyMap : Dictionary<string, string>
    {
        public new string this[string key]
        {
            get
            {
                if (base.ContainsKey(key))
                    return base[key];
                else
                    return string.Empty;
            }
            set
            {
                base[key] = value;
            }
        }

        public override string ToString()
        {
            string toReturn = string.Empty;

            foreach(string key in base.Keys)
            {
                toReturn += key + ": " + base[key] + Environment.NewLine;
            }

            return toReturn;
        }
    }
}
