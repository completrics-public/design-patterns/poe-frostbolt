﻿using Frostbolt.SkillGems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Frostbolt.State.Components
{
    public class SkillGroups : List<List<ISkillGem>>
    {
        internal List<ISkillGem> Where()
        {
            throw new NotImplementedException();
        }
    }

    public static class SkillGroupsFactory
    {
        public static SkillGroups CreateSkillGroups(GameState gameState)
        {
            SkillGroups skillGroups = new SkillGroups();

            // Step 1: get all ACTIVE gems from ALL equipment (in our case, only body armour)
            ISkillGem[] activeGems = gameState.GemSlots.Values.Where(g => g.Tags.Contains("active")).ToArray();
            foreach(var gem in activeGems)
            {
                skillGroups.Add(new List<ISkillGem>() { gem });
            }

            // Step 2: for each ACTIVE gem, find all supports which match
            foreach(var skillGroup in skillGroups)
            {
                ISkillGem activeGem = skillGroup[0];
                ISkillGem[] possibleSupportGems = gameState.GemSlots.Values.Where(g => g.Tags.Contains("support")).ToArray();

                foreach(var potentialGem in possibleSupportGems)
                {
                    string[] ag = activeGem.Tags;
                    string[] pg = potentialGem.Tags;

                    // Now - how it works: POTENTIAL gems cannot have tags exceeding ACTIVE gems (the opposite is 100% ok)
                    string[] difference = pg.Except(ag).ToArray();
                    if (difference.Length == 1)
                    {
                        // means we have Support tag only
                        skillGroup.Add(potentialGem);
                    }
                }
            }

            return skillGroups;
        }
    }
}
