﻿using Frostbolt.SkillGems;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.State.Components.Equipment
{
    public class BodyArmour : Dictionary<int, ISkillGem>
    {
        public BodyArmour()
        {
            // Body Armour can have up to 6 slots. Indexing from 1, simply for convenience.
            for(int i=1; i<7;i++)
            {
                this[i] = SkillGemFactory.Create("unallocated");
            }
        }

        public new ISkillGem this[int key]
        {
            get
            {
                if (base.ContainsKey(key))
                    return base[key];
                else
                    return SkillGemFactory.Create("unallocated");   //NullObject, no influence on the system
            }
            set
            {
                base[key] = value;
            }
        }

        public override string ToString()
        {
            string toReturn = string.Empty;

            for(int i=1; i<7; i++)
            {
                toReturn += i.ToString() + ": " + this[i].ToString() + Environment.NewLine;
            }

            return toReturn;
        }
    }
}
