﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Frostbolt.State.Components
{
    public class ActionResult : Dictionary<string, string>
    {
        public new string this[string key]
        {
            get
            {
                if (base.ContainsKey(key))
                    return base[key];
                else
                    return string.Empty;
            }
            set
            {
                base[key] = value;
            }
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
