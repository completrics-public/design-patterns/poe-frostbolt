﻿using Frostbolt.Engine;
using Frostbolt.State;
using Frostbolt.Ui;
using System;

namespace Frostbolt
{
    class Program
    {
        // setgem 1 icenova
        // setgem 2 gmp
        // setgem 3 frostbolt
        // setgem 4 spellecho
        // setgem 5 cd
        // setbutton mouse right frostbolt
        // setbutton key t frostbolt
        // press key t
        // press mouse right

        static void Main(string[] args)
        {
            GameEngine gameEngine = new GameEngine();
            GameState gameState = new GameState();

            while(gameState.Finished == false)
            {
                string instructionString = Console.ReadLine();
                string[] instruction = instructionString.Split(" ");

                gameEngine.Process(instruction, gameState);

                Console.Out.Write(new UiMessageDisplayer().Display(gameState));

            }
        }
    }
}
