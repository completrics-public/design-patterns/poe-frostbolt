﻿using Frostbolt.State;
using Frostbolt.State.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.SkillGems
{
    public interface ISkillGem
    {
        string Name { get; }
        string[] Tags { get; }

        void PerformAction(ActionResult actionState, GameState gameState);
    }
}
