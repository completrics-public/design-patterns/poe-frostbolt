﻿using System;
using System.Collections.Generic;
using System.Text;
using Frostbolt.State;
using Frostbolt.State.Components;

namespace Frostbolt.SkillGems
{
    public class GreaterMultipleProjectileSupport : ISkillGem
    {
        public string Name => "gmp";

        public string[] Tags => new string[] { "spell", "projectile", "support" };

        public override bool Equals(object obj)
        {
            if (!(obj is GreaterMultipleProjectileSupport other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // We know we are dealing with projectile-based Skill Gem, therefore we can get it:
            int projectileCount = int.Parse(actionState["projectiles"]);
            actionState["projectiles"] = (projectileCount * 5).ToString();

            // We need to add the spread via angles
            actionState["angles"] = "-10, -5, 0, 5, 10";

            // We need to reduce the damage per bolt to 75% of original
            int damage = int.Parse(actionState["damage"]);
            actionState["damage"] = ((int)(damage * 0.75)).ToString();
        }
    }
}
