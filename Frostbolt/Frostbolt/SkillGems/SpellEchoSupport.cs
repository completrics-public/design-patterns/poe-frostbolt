﻿using Frostbolt.State;
using Frostbolt.State.Components;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Frostbolt.SkillGems
{
    public class SpellEchoSupport : ISkillGem
    {
        public string Name => "spellecho";

        public string[] Tags => new string[] { "spell", "support" };

        public override bool Equals(object obj)
        {
            if (!(obj is SpellEchoSupport other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // We need to get the amount of casts / projectiles.
            // This below should be a function (and a bit more resilient), but for the purpose of the video I leave it as it is
            if (int.TryParse(actionState["casts"], out int casts) == false)
                casts = 0;
            if (casts > 0) 
                actionState["casts"] = (casts * 2).ToString();

            if (int.TryParse(actionState["projectiles"], out int projectiles) == false)
                projectiles = 0;
            if (projectiles > 0) 
                actionState["projectiles"] = (projectiles * 2).ToString();

            // Aside from that, increase cast time
            double castSpeed = double.Parse(actionState["cast speed"], NumberStyles.Any, CultureInfo.InvariantCulture);
            actionState["cast speed"] = (castSpeed / 1.7).ToString("0.00", CultureInfo.InvariantCulture);

            // And reduce damage
            int damage = int.Parse(actionState["damage"]);
            actionState["damage"] = (damage * 0.9).ToString();
        }
    }
}
