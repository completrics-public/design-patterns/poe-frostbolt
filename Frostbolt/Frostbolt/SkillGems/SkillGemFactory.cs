﻿using Frostbolt.SkillGems;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.SkillGems
{
    public static class SkillGemFactory
    {
        public static ISkillGem Create(string gemType)
        {
            if (gemType.ToLower() == "frostbolt") return new FrostBolt();
            if (gemType.ToLower() == "icenova") return new IceNova();
            if (gemType.ToLower() == "gmp") return new GreaterMultipleProjectileSupport();
            if (gemType.ToLower() == "spellecho") return new SpellEchoSupport();
            if (gemType.ToLower() == "cd") return new ControlledDestructionSupport();
            return new UnallocatedSkillGem();
        }
    }
}
