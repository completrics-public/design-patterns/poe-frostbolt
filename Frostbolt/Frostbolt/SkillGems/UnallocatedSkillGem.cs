﻿using Frostbolt.State;
using Frostbolt.State.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.SkillGems
{
    public class UnallocatedSkillGem : ISkillGem
    {
        public string Name => "none";

        public string[] Tags => new string[] { };

        public override bool Equals(object obj)
        {
            if (!(obj is UnallocatedSkillGem other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // Do nothing at all
            return;
        }

        public override string ToString()
        {
            return "empty";
        }
    }
}
