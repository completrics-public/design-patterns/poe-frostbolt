﻿using Frostbolt.State;
using Frostbolt.State.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.SkillGems
{
    public class FrostBolt : ISkillGem
    {
        public string Name => "frostbolt";

        public string[] Tags => new string[] { "spell", "projectile", "active" };

        public override bool Equals(object obj)
        {
            if (!(obj is FrostBolt other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // This is an active skill gem.
            // It is guaranteed to go first.

            actionState.Add("projectiles", "1");
            actionState.Add("damage", "2000");
            actionState.Add("cast speed", "0.75");
        }
    }
}
