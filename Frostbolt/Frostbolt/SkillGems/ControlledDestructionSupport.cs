﻿using System;
using System.Collections.Generic;
using System.Text;
using Frostbolt.State;
using Frostbolt.State.Components;

namespace Frostbolt.SkillGems
{
    public class ControlledDestructionSupport : ISkillGem
    {
        public string Name => "cd";

        public string[] Tags => new string[] { "spell", "support" };

        public override bool Equals(object obj)
        {
            if (!(obj is ControlledDestructionSupport other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // We need to multiply the damage of the spell by 1.4
            int damage = int.Parse(actionState["damage"]);
            actionState["damage"] = ((int)(damage * 1.4)).ToString();

            // We need to reduce BASE critical chance by 100%
            actionState["base critical damage"] = "-100";
        }
    }
}
