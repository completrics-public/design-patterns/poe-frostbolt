﻿using System;
using System.Collections.Generic;
using System.Text;
using Frostbolt.State;
using Frostbolt.State.Components;

namespace Frostbolt.SkillGems
{
    public class IceNova : ISkillGem
    {
        public string Name => "icenova";

        public string[] Tags => new string[] { "spell", "active", "aoe" };

        public override bool Equals(object obj)
        {
            if (!(obj is IceNova other)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Tags);

        public void PerformAction(ActionResult actionState, GameState gameState)
        {
            // This is an active skill gem.
            // It is guaranteed to go first.

            actionState.Add("casts", "1");
            actionState.Add("damage", "800");
            actionState.Add("cast speed", "0.7");
        }
    }
}
