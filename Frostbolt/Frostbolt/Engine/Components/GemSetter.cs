﻿using Frostbolt.SkillGems;
using Frostbolt.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.Engine
{
    public class GemSetter
    {
        /// <summary>
        /// Input gems to slots.
        /// </summary>
        /// <param name="instruction">[0]: setgem, [1]: position, [2]: gem type</param>
        /// <param name="gameState"></param>
        public void SetGem(string[] instruction, GameState gameState)
        {
            int position = int.Parse(instruction[1]);
            string gemType = instruction[2];

            ISkillGem gem = SkillGemFactory.Create(gemType);

            gameState.GemSlots[position] = gem;
        }
    }
}
