﻿using Frostbolt.SkillGems;
using Frostbolt.State;
using Frostbolt.State.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Frostbolt.Engine
{
    public class ActionProcessor
    {
        public void Process(string[] instruction, GameState gameState)
        {
            string keyPressed = instruction[1] + " " + instruction[2];

            // Step 1: find which SkillGem was used
            string skillGemType = gameState.Keys[keyPressed];

            // Step 2: build Skill Groups from skill gems; use only ACTIVE gems
            gameState.BuildSkillGroupsFromActiveGems();

            // Step 3: get a PROPER Skill Group
            List<ISkillGem> properGroup = gameState.ActiveSkillGroups.Where(l => l[0].Name.Equals(skillGemType)).First();

            // Step 4: activate the Skill Group
            ActionResult result = ProcessSkillGroup(properGroup, gameState);

            // Set the result of the Skill Group as the active group
            gameState.ActionResult = result;
        }

        private ActionResult ProcessSkillGroup(List<ISkillGem> skillGroup, GameState gameState)
        {
            // Time to run the chain of responsibility

            ActionResult actionState = new ActionResult();

            foreach(ISkillGem gem in skillGroup)
            {
                gem.PerformAction(actionState, gameState);
            }

            return actionState;
        }
    }
}
