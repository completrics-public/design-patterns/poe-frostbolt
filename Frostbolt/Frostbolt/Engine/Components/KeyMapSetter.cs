﻿using Frostbolt.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.Engine
{
    public class KeyMapSetter
    {
        public void SetKey(string[] instruction, GameState gameState)
        {
            string position = instruction[1] + " " + instruction[2];
            string skillGemType = instruction[3];

            gameState.Keys[position] = skillGemType;
        }
    }
}
