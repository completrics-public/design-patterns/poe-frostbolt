﻿using Frostbolt.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Frostbolt.Engine
{
    public class GameEngine
    {
        public void Process(string[] instruction, GameState gameState)
        {
            if (instruction[0].ToLower() == "setgem")
            {
                new GemSetter().SetGem(instruction, gameState);
                gameState.Message = "Gems set" + Environment.NewLine + Environment.NewLine + gameState.GemSlots.ToString() + Environment.NewLine;
            }
            else if (instruction[0].ToLower() == "setbutton")
            {
                new KeyMapSetter().SetKey(instruction, gameState);
                gameState.Message = "Key set" + Environment.NewLine + Environment.NewLine + gameState.Keys.ToString() + Environment.NewLine;
            }
            else if (instruction[0].ToLower() == "exit")
            {
                gameState.Finished = true;
                gameState.Message = "Game finished";
            }
            else if (instruction[0].ToLower() == "press")
            {
                new ActionProcessor().Process(instruction, gameState);
                gameState.Message = gameState.ActionResult + Environment.NewLine;
            }
            else
            {
                gameState.Message = "Unable to comply: " + instruction[0] + Environment.NewLine + Environment.NewLine;
            }
        }
    }
}
