## Body Armour links

1. B: **Frostbolt** (spell, 2k damage, projectile, cast time 0.75s)
2. B: **Spell Echo Support** (*0.9 damage, repeat, *1.7 cast speed)
3. G: **Greater Multiple Projectiles Support** (*0.75 damage, 4 additional projectiles)
4. B: **Ice Nova** (spell, 800 damage, AoE, cast time 0.7s)
5. B: **Controlled Destruction** (-% crit chance, *1.4 damage)
6. B: empty

## Actual Skill Groups

* RIGHT MOUSE BUTTON: 
    * Frostbolt (1) - SE (2) - GMP (3) - CD (5)  
    * _projectiles_: 10  
    * _angles_: -10, -5, 0, 5, 10
    * _damage_: (2000 * 0.75 * 0.9 * 1.4) = 1890 
    * _cast time_: (0.75s/1.7) = 0.44s
* MIDDLE MOUSE BUTTON: 
    * Ice Nova (4) - SE (2) - CD (5) 
    * _casts_: 2
    * _damage_ (800 * 0.9 * 1.4) = 2128
    * _cast time_ (0.7/1.7) = 0.41s

## Phases:

1. Build Skill Groups
2. Use Skill Groups
